﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{

    private void Awake()
    {
        int numMusicPlayer = FindObjectsOfType<MusicPlayer>().Length;
        print("Number of music player in this scene " + numMusicPlayer);
        // if more than one music player in scene
        if (numMusicPlayer > 1)
        {
            // destroy ourselves
            Destroy(gameObject);
        }
        else
        {
            // else
            DontDestroyOnLoad(gameObject);
            // DontDestroyOnLoad(this); Also the same
        }

    }


}
